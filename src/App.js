import React, { Component } from 'react'
import './App.css'
import {} from 'bootstrap-4-react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { withRouter } from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <div className="">
        <Router history={this.props.history}>
          {/* Место для header */}
          <div className={'container mt-5'}>
            <div className={'row mt-5'}>
              <div className={'col-12 text-center mt-5'}>
                Ваш проект поднялся, приступайте изучению HTML, CSS, REACT
              </div>
            </div>
          </div>
          <Switch>
            {/* <Route path="/security" component={Security} />
             */}
          </Switch>
          {/* Место для Footer */}
        </Router>
      </div>
    )
  }
}

export default withRouter(App)
